module gitlab.com/polyapp-open-source/JSON2App

go 1.14

require (
	cloud.google.com/go/firestore v1.5.0 // indirect
	cloud.google.com/go/storage v1.15.0 // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	gitlab.com/polyapp-open-source/polyapp v0.0.0-20210510222527-550829b327c3
	golang.org/x/mod v0.4.2 // indirect
	golang.org/x/net v0.0.0-20210505214959-0714010a04ed // indirect
	golang.org/x/sys v0.0.0-20210503173754-0981d6026fa6 // indirect
	google.golang.org/api v0.46.0 // indirect
	google.golang.org/genproto v0.0.0-20210505142820-a42aa055cf76 // indirect
)
